module.exports = function(database, schemas) {
    var populationSchema = database.Schema({
        'type' : {type : String},
        'size' : {type : Number, min : 0},
        'age' : {type : Number, min : 0},
        'regionId' : {type : database.Schema.Types.ObjectId}
    });

    database.model('population', populationSchema, 'population');

    return populationSchema;
};
