var crypto = require('crypto');

module.exports = function(database, schemas) {
    // Get current date
    var now = Date.now();

    // Define the Schema
    var playerSchema = database.Schema({
        username : { type: String, default: '' },
        provider : { type: String, default: '' },
        hashed_password : { type: String, default: '' },
        salt : { type: String, default: '' },
        authToken : { type: String, default: '' },
        isAdmin : { type: Boolean, default: false },
        created : { type: Date, default: now },
        updated : { type: Date, default: now },
        lastLogin : { type: Date, default: now },
        lastActive : { type: Date, default: now },
    });

    // Virtual methods
    playerSchema.virtual('password')
        .set(function(password) {
            this._password = password;
            this.salt = this.makeSalt();

            var hashedPassword = this.encryptPassword(password);

            this.hashed_password = this.encryptPassword(password);
        })
        .get(function() { return this._password });

    // Validation for certain fields
    playerSchema.path('username').validate(function (username) {
        if (this.skipValidation()) return true;
        return username.length;
    }, 'Username cannot be blank');

    playerSchema.path('hashed_password').validate(function (hashed_password) {
        if (this.skipValidation()) return true;
        return hashed_password.length;
    }, 'Password cannot be blank');

    // Custom methods
    playerSchema.methods = {
        authenticate: function (plainText) {
            return this.encryptPassword(plainText) === this.hashed_password;
        },

        makeSalt: function () {
            return Math.round((new Date().valueOf() * Math.random())) + '';
        },

        encryptPassword: function (password) {
            if (!password) return '';

            try {
                return crypto
                    .createHmac('sha1', this.salt)
                    .update(password)
                    .digest('hex');
            } catch (err) {
                return '';
            }
        },

        skipValidation: function() {
            return false;
        }
    };

    // Set up the actual model from the schema and send it back to the calling script
    var player = database.model('player', playerSchema, 'player');

    return playerSchema;
};
