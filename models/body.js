module.exports = function(database, schemas) {
    var bodySchema = database.Schema({
        'name' : String,
        'systemId' : database.Schema.Types.ObjectId
    });

    database.model('body', bodySchema, 'body');

    return bodySchema;
};
