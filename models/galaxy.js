module.exports = function(database, schemas) {
    var galaxySchema = database.Schema({
        'name' : String,
        'universeId' : database.Schema.Types.ObjectId
    });

    database.model('galaxy', galaxySchema, 'galaxy');

    return galaxySchema;
};
