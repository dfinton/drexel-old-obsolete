module.exports = function(database, schemas) {
    // Define the Schema
    var characterSchema = database.Schema({
        name : { type: String, default: '' },
    });

    // Set up the actual model fromt he schema and send it back to the calling script
    var character = database.model('character', characterSchema, 'character');

    return characterSchema;
};
