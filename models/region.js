module.exports = function(database, schemas) {
    var regionSchema = database.Schema({
        'name' : String,
        'bodyId' : database.Schema.Types.ObjectId
    });

    database.model('region', regionSchema, 'region');

    return regionSchema;
};
