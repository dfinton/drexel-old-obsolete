module.exports = function(database) {
    var schemaLabels = [
        'process-id',
        'population',
        'region',
        'body',
        'system',
        'galaxy',
        'universe',
        'character',
        'player',
    ];

    var schemas = {};

    for (var i = 0; i < schemaLabels.length; i++) {
        var schemaLabel = schemaLabels[i];
        var requirePath = './' + schemaLabel;

        schemas[schemaLabel] = require(requirePath)(database, schemas);
    }
}
