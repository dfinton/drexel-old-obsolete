var exec = require('child_process').exec;

module.exports = function(database, schemas) {
    var ProcessIDSchema = database.Schema({
        processID : { type : Number, default : 0 },
        startTime : { type: Date, default: Date.now },
    });

    var processID = database.model('processID', ProcessIDSchema, 'processID');

    return ProcessIDSchema;
};
