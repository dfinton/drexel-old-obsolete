module.exports = function(database, schemas) {
    var universeSchema = database.Schema({
        'name' : String
    });

    database.model('universe', universeSchema, 'universe');

    return universeSchema;
};
