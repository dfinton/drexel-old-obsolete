module.exports = function(database, schemas) {
    var systemSchema = database.Schema({
        'name' : String,
        'galaxyId' : database.Schema.Types.ObjectId
    });

    database.model('system', systemSchema, 'system');

    return systemSchema;
};
