// Load up our core libraries
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var debug = require('debug')('drexel:debug');
var domain = require('domain');
var express = require('express');
var flash = require('express-flash');
var handlebars = require('express-handlebars');
var logger = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var path = require('path');
var session = require('express-session');

// Declare the app variable so we can start working
var app = express();

// Load up our application libraries
var appBuilder = require('./lib/appBuilder');

// Load up our configuration
var databaseConfig = require('./config/database')(app.get('env'));
var restConfig = require('./config/rest');
var passportConfig = require('./config/passport');
var sessionConfig = require('./config/session');
var drexelConfig = require('./config/drexel');

// view engine setup
app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Initialize some libraries
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(flash());
app.use(session(sessionConfig));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('./public'));

// Initialize our database
var database = mongoose.connect(databaseConfig);

// Initialize our models
require('./models')(database);

// Set up middleware to filter for authentication, initialization, etc
var middleware = require('./middleware')(database);

// Set up passport for player session/auth management
passportStrategy = passportConfig(database.model('player'));

passport.serializeUser(function(player, done) {
    done(null, { 
        _id : player.id,
        username : player.username,
        isAdmin : player.isAdmin,
        created : player.created,
        updated : player.updated,
        lastLogin : player.lastLogin,
        lastActive : player.lastActive,
    });
});

passport.deserializeUser(function(playerObj, done) {
    database.model('player').find({ criteria: { _id: playerObj._id } }, function (err, player) {
        done(err, player);
    });
});

passport.use(passportStrategy);

// REST API
appBuilder.rest(app, debug, database, restConfig, drexelConfig);

// Main web pages
appBuilder.routes(app, debug, middleware, database, passport, drexelConfig);

// Pass our app object to the calling script
module.exports = app;
