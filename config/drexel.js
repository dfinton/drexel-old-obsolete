module.exports = {
    validEntityList : [
        'universe',
        'galaxy',
        'system',
        'body',
        'region'
    ],

    entityHierarchyMap : {
        universe : 'galaxy',
        galaxy : 'system',
        system : 'body',
        body : 'region',
    }
}
