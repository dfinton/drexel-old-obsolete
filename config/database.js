var databaseConfig = {
    development : "mongodb://localhost/drexel"
};

module.exports = function(envStr) {
    return databaseConfig[envStr];
};
