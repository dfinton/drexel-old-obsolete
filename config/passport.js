var LocalStrategyClass = require('passport-local').Strategy;

var localStrategyFields = {
    usernameField: 'username',
    passwordField: 'password'
};

module.exports = function(playerModel) {
    var localStrategyMethod = function(username, password, done) {
        var options = { username: username };

        playerModel.findOne(options, function(err, player) {
            if (err) return done(err);

            if (player.length === 0) {
                return done(null, false, { message: 'Unknown player' });
            }

            if (!player.authenticate(password)) {
                return done(null, false, { message: 'Invalid password' });
            }

            return done(null, player);
        });
    };

    var localStrategy = new LocalStrategyClass(localStrategyFields, localStrategyMethod);

    return localStrategy;
};
