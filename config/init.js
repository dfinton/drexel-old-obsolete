var drexelInit = {
    admin : 'admin',
    player : 'player',
};

module.exports = function(drexelProperty) {
    if (drexelInit.hasOwnProperty(drexelProperty) {
        return drexelInit[drexelProperty];
    } else {
        return null;
    }
}
