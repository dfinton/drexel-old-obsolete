module.exports = function(database) {
    var requiresLogin = function(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }

        if (req.method == 'GET') {
            req.session.returnTo = req.originalUrl;
        }

        res.redirect('/login');
    };

    return requiresLogin;
};
