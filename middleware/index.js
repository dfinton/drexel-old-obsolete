module.exports = function(database) {
    var requiresAdminInit = require('./requires-admin-init')(database);
    var requiresUniverseInit = require('./requires-universe-init')(database);
    var requiresLogin = require('./requires-login')(database);
    var requiresAdmin = require('./requires-admin')(database);
    var updatePlayerActivity = require('./update-player-activity')(database);

    var middleware = {
        requiresAdminInit : requiresAdminInit,
        requiresUniverseInit : requiresUniverseInit,
        requiresLogin : requiresLogin,
        requiresAdmin : requiresAdmin,
        updatePlayerActivity : updatePlayerActivity,
    };

    return middleware;
};
