module.exports = function(database) {
    var requiresAdmin = function(req, res, next) {
        if (req.isAuthenticated() && req.session.passport.user.isAdmin) {
            return next();
        } else {
            var err = new Error('Unauthorized');
            err.status = 401;
            return next(err);
        }
    };

    return requiresAdmin;
};
