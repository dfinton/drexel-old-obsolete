module.exports = function(database) {
    var updatePlayerActivity = function(req, res, next) {
        if (req.isAuthenticated()) {
            database.model('player').findById(req.session.passport.user._id, function(err, player) {
                if (err) {
                    return next(err);
                } else {
                    var now = Date.now();

                    player.lastActive = now;

                    player.save(function(err) {
                        if (err) {
                            return next(err);
                        } else {
                            return next();
                        }
                    });
                }
            });
        } else {
            return next();
        }
    };

    return updatePlayerActivity;
};
