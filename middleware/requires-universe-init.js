module.exports = function(database) {
    var requiresUniverseInit = function(req, res, next) {
        database.model('universe').count({}, function(err, uCount) {
            if (err) {
                return next(err);
            } else {
                if (0 === uCount) {
                    res.redirect('/init-universe');
                } else {
                    return next();
                }
            }
        });
    };

    return requiresUniverseInit;
};
