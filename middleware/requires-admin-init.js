module.exports = function(database) {
    var requiresAdminInit = function(req, res, next) {
        database.model('player').count({ isAdmin: true }, function(err, aCount) {
            if (err) {
                return next(err);
            } else {
                if (0 === aCount) {
                    res.redirect('/init-player');
                } else {
                    return next();
                }
            }
        });
    };

    return requiresAdminInit;
};
