var padString = function(string, width, padChar) {
    if ('string' !== typeof(string)) {
        string = string.toString();
    }

    var padLength = Math.max (0, width - string.length);
    var padString = new Array(padLength + 1).join(padChar);

    return padString + string;
}

var convertDateToString = function(dateString) {
    date = new Date(dateString);

    var months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var calendarDate = date.getDate();
    var hourMil = date.getHours();
    var hour = padString(hourMil % 12, 2, 0);
    var AMorPM = hourMil < 12 ? 'AM' : 'PM';
    var minute = padString(date.getMinutes(), 2, 0);
    var second = padString(date.getSeconds(), 2, 0);

    hour = 0 === hour ? 12 : hour;

    var dateString = month + ' ' + calendarDate + ', ' + year + ' at ' + hour + ':' + minute + ':' + second + ' ' + AMorPM;

    return dateString;
};

var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var server = {
    ajaxURL : '/rest/daemon',

    serverStatusDisplayElem : $('div.drexel.server-status'),
    serverStatusToggleButton : $('button.drexel.server-status-toggle'),

    init : function() {
        if (0 === $('div.drexel.primary').length) {
            return;
        }

        this.resetDisplay();
        this.getServerStatus();

        this.serverStatusToggleButton.on('click', function(event) {
            event.preventDefault();
            server.toggleServerStatus($(event.target));
        });
    },

    resetDisplay : function() {
        this.serverStatusDisplayElem.html('Obtaining server status...');
        this.serverStatusToggleButton.attr('data-server-status', 'unknown');
        this.serverStatusToggleButton.attr('disabled', 'disabled');
    },

    displayServerStatus : function(data) {
        if (data.isRunning) {
            this.serverStatusDisplayElem.html('The server is running and was started on ' + convertDateToString(data.startTime));
            this.serverStatusToggleButton.attr('data-server-status', 'up');
            this.serverStatusToggleButton.removeAttr('disabled');
            this.serverStatusToggleButton.html('Stop Server');
        } else {
            this.serverStatusDisplayElem.html('The server is not running');
            this.serverStatusToggleButton.attr('data-server-status', 'down');
            this.serverStatusToggleButton.removeAttr('disabled');
            this.serverStatusToggleButton.html('Start Server');
        }
    },

    toggleServerStatus : function(target) {
        var serverStatusAttr = target.attr('data-server-status');
        var postData = {};

        if ('up' === serverStatusAttr) {
            postData.isRunning = false;
        } else if ('down' === serverStatusAttr) {
            postData.isRunning = true;
        }

        if ('unknown' !== serverStatusAttr) {
            this.resetDisplay();

            $.post(this.ajaxURL, postData).done(function(data) {
                server.displayServerStatus(data);
            });
        }
    },

    getServerStatus : function() {
        $.get(this.ajaxURL, function(data, status) {
            server.displayServerStatus(data);
        });
    }
};

var entitySummary = {
    entitySummaryAjaxURL : '/rest/entity-summary',
    adminDiv : $('div.drexel.primary.admin'),

    init : function() {
        if (0 === this.adminDiv.length) {
            return;
        }
    },

    loadEntity : function(event) {
        var ajaxData = {
            ent : $(this).attr('data-entity-type'),
            id : $(this).attr('data-entity-id')
        };

        $.ajax({
            type : 'GET',
            url : entitySummary.entitySummaryAjaxURL,
            data : ajaxData,
            context: entitySummary
        }).done(entitySummary.populateAdminDiv);
    },

    populateAdminDiv : function(data) {
        if (! data.hasOwnProperty('_id')) {
            return;
        }

        // Generate the main header
        var adminHeader = $('<h2>')
            .html(data.name);

        this.adminDiv.empty();
        this.adminDiv.append(adminHeader);

        // Generate the population chart area
        for (var index = 0, len = data.populationTypeList.length; index < len; index++) {
            var populationType = data.populationTypeList[index];
            var populationHeaderString = "No " + populationType + " Population Found";

            if (data.populationData.Human.length > 0) {
                populationHeaderString = populationType + " Population Summary (Total: " + numberWithCommas(data.totalPopulationByType[populationType]) + ",000)";
            }

            var populationHeader = $('<h3>')
                .html(populationHeaderString);

            var populationChartDivId = 'population-chart-' + populationType.toLowerCase();
            var populationChartDiv = $('<div>')
                .addClass('drexel population-chart')
                .attr('id', populationChartDivId);

            this.adminDiv.append(populationHeader);
            this.adminDiv.append(populationChartDiv);

            var populationChart = $.jqplot(populationChartDivId, [data.populationData[populationType]], {
                grid: {
                    drawBorder: false,
                    shadow: false,
                    background: 'rgba(0,0,0,0)'
                },
                highlighter: { show: true },
                seriesDefaults: { 
                    shadowAlpha: 0.1,
                    shadowDepth: 2,
                    fillToZero: true
                },
                series: [
                    {
                        color: '#337ab7',
                        showLine: true,
                        fill: true,
                        fillAndStroke: false,
                        rendererOptions: {
                            smooth: true
                        }
                    }
                ],
                axes: {
                    xaxis: {
                        // padding of 0 or of 1 produce same results, range 
                        // is multiplied by 1x, so it is not padded.
                        pad: 1.0,
                        tickOptions: {
                          showGridline: false
                        },
                        min: 0
                    },
                    yaxis: {
                        pad: 1.05,
                        min: 0
                    }
                }
            });
        }
    }
}

var entityTree = {
    entityTreeAjaxURL : '/rest/entity-tree',
    entityTreeDiv : $('div.drexel.entity-tree'),

    init : function() {
        if (0 === this.entityTreeDiv.length) {
            return;
        }

        var ajaxData = {};

        $.ajax({
            type : 'GET',
            url : this.entityTreeAjaxURL,
            data : ajaxData,
            context: this
        }).done(this.refreshTree);
    },

    getTreeData : function(event) {
        if ($(this).attr('data-is-loaded') !== 'false') {
            return;
        }

        var ajaxData = {
            ent : $(this).attr('data-entity-type'),
            id : $(this).attr('data-entity-id')
        };

        $.ajax({
            type : 'GET',
            url : entityTree.entityTreeAjaxURL,
            data : ajaxData,
            context: entityTree
        }).done(entityTree.populateTree);

        $(this).attr('data-is-loaded', 'true');
    },

    generateAccordianDiv : function(entities, hierMap, entityType, isSubAccordion) {
        if (typeof(isSubAccordion) == 'undefined') {
            isSubAccordion = false;
        }

        var isLoadedParam = null;

        // If it's a top-level node, or if the node is less than 3 rungs from the 
        // bottom of the hierarchy, no loading is necessary. In all other cases, 
        // we make an AJAX call on click
        if (! (isSubAccordion && hierMap.hasOwnProperty(entityType) && hierMap.hasOwnProperty(hierMap[entityType]))) {
            isLoadedParam = 'true';
        } else {
            isLoadedParam = 'false';
        }

        var parentDiv = $('<div>');

        if (isSubAccordion) {
            parentDiv.addClass('drexel tree-indent');
        }

        entities.forEach(function(entity) {
            // create the header of the accordian
            var link = $('<a>').addClass('accordion-toggle btn btn-primary drexel tree-item')
                .attr('data-toggle', 'collapse')
                .attr('data-parent', '#' + entityType + '-tree-accordion')
                .attr('data-entity-type', entityType)
                .attr('data-entity-id', entity._id)
                .attr('data-is-loaded', isLoadedParam)
                .attr('role', 'button')
                .attr('href', '#' + entityType + '-collapse-' + entity._id)
                .html(entity.name)
                .on('click', entityTree.getTreeData)
                .on('click', entitySummary.loadEntity);

            var heading = $('<div>').addClass('accordion-heading')
                .append(link);

            // create the body of the accordian
            var body = $('<div>').addClass('accordion-body collapse')
                .attr('id', entityType + '-collapse-' + entity._id);

            var childEntityType = hierMap[entityType];
            var childEntityGroupName = childEntityType + 'List';

            if (entity.hasOwnProperty(childEntityGroupName)) {
                var childAccordions = entityTree.generateAccordianDiv(entity[childEntityGroupName], hierMap, childEntityType, true);

                body.html(childAccordions);
            }

            // add the header and the body to the main accordian group
            var group = $('<div>').addClass('accordion-group')
                .append(heading)
                .append(body);

            var accordion = $('<div>').addClass('accordion')
                .attr('id', entityType + '-tree-accordion')
                .append(group);

            // Add it into the main parent div
            if (isSubAccordion) {
                var inner = $('<div>').addClass('accordion-inner')
                    .html(accordion);

                parentDiv.append(inner);
            } else {
                parentDiv.append(accordion);
            }
        });

        return parentDiv;
    },

    refreshTree : function(data) {
        this.entityTreeDiv.html(this.generateAccordianDiv(data.entities, data.hierMap, data.entityType));
    },

    populateTree : function(data) {
        var entity = data.entities[0];

        var childEntityType = data.hierMap[data.entityType];
        var childEntityGroupName = childEntityType + 'List';
        var childEntityList = entity[childEntityGroupName];

        childEntityList.forEach(function(childEntity) {
            var childEntityId = childEntity._id;
            var childEntityAccordionBody = $('.accordion-toggle[data-entity-id="' + childEntityId + '"]')
                .closest('.accordion-group')
                .children('.accordion-body');

            var grandchildEntityType = data.hierMap[childEntityType];
            var grandchildEntityGroupName = grandchildEntityType + 'List';
            var grandchildEntityList = childEntity[grandchildEntityGroupName];

            var grandchildAccordians = entityTree.generateAccordianDiv(grandchildEntityList, data.hierMap, grandchildEntityType, true);

            childEntityAccordionBody.html(grandchildAccordians);
        });
    }
}

var keepAlive = {
    ajaxURL : '/rest/keepalive',
    intervalMinutes : 5,
    timerID : null,

    init : function() {
        setInterval(function() {

        }, this.intervalMinutes * 60000);
    }
};

$(document).ready(function() {
    server.init();
    // keepAlive.init();
    entitySummary.init();
    entityTree.init();
});
