module.exports = function(param) {
    var Player = param.database.model('player');

    param.app.post(param.urlPath, function(req, res, next) {
        var username = req.body['username'];
        var password = req.body['password'];

        var player = new Player({
            username : username,
            password : password,
            isAdmin : true,
        });

        player.save(function(err) {
            if (err) {
                return next(err);
            } else {
                res.redirect('/');
            }
        });
    });

    param.app.get(param.urlPath, function(req, res, next) {
        res.render(param.viewPath, {});
    });
};
