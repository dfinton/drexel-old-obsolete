module.exports = function(param) {
    var loginSuccess = function (req, res, next) {
        var redirectTo = req.session.returnTo ? req.session.returnTo : '/';

        param.database.model('player').findById(req.session.passport.user._id, function(err, player) {
            if (err) {
                return next(err);
            } else {
                var now = Date.now();

                player.lastLogin = now;

                player.save(function(err) {
                    if (err) {
                        return next(err);
                    } else {
                        delete req.session.returnTo;
                        res.redirect(redirectTo);
                    }
                });
            }
        });

    };

    var authenticateParams = {
        failureRedirect : param.urlPath,
        failureFlash : true
    };

    param.app.use(param.middleware.requiresAdminInit);
    param.app.post(param.urlPath, param.passport.authenticate('local', authenticateParams), loginSuccess);

    param.app.get(param.urlPath, function (req, res) {
        res.render(param.viewPath);
    });
};
