module.exports = function(param) {
    param.app.get(param.urlPath, param.middleware.requiresLogin);
    param.app.get(param.urlPath, param.middleware.requiresAdmin);
    param.app.get(param.urlPath, param.middleware.requiresUniverseInit);

    param.app.get(param.urlPath, function(req, res, next) {
        res.render(param.viewPath, {});
    });
};
