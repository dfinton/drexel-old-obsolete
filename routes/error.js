var error404Handler = function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
};

var errorHandler = function(app, urlPath, viewPath) {
    var errorHandlerHelper = function(err, req, res, next) {
        var errorStatus = err.status || 500;
        var stackTrace = app.get('env') === 'development' ? err.stack.replace(/(?:\r\n|\r|\n)/g, '<br />') : {};

        res.status(errorStatus);
        res.render(viewPath, {
            message : err.message,
            status : errorStatus,
            error : stackTrace
        });
    };

    return errorHandlerHelper;
};

module.exports = function(param) {
    param.app.use(param.middleware.requiresLogin);
    param.app.use(error404Handler);
    param.app.use(errorHandler(param.app, param.urlPath, param.viewPath));
};
