module.exports = function(param) {
    var Population = param.database.model('population');
    var Region = param.database.model('region');
    var Body = param.database.model('body');
    var System = param.database.model('system');
    var Galaxy = param.database.model('galaxy');
    var Universe = param.database.model('universe');

    param.app.get(param.urlPath, param.middleware.requiresLogin);

    var populationType = 'Human';
    var populationSize = 10000;
    var populationMaxAge = 90;
    var entityNum = 3;
    
    param.app.post(param.urlPath, function(req, res, next) {
        var generateUniverseData = function() {
            for (var universeIndex = 0; universeIndex < entityNum; universeIndex++) {
                var universeName = req.body['universe-name'] + ' ' + universeIndex;
                var galaxyList = [];

                var universe = new Universe({
                    name : universeName
                });

                universe.save(function(err, universe) {
                    if (err) {
                        return next(err);
                    } else {
                        generateGalaxyData(universe, universe.id.slice(-1));
                    }
                });
            }
        };

        var generateGalaxyData = function(universe, universeIndex) {
            for (var galaxyIndex = 0; galaxyIndex < entityNum; galaxyIndex++) {
                var galaxyName = 'Galaxy ' + universeIndex + '-' + galaxyIndex;

                var galaxy = new Galaxy({
                    name : galaxyName,
                    universeId : universe._id
                });

                galaxy.save(function(err, galaxy) {
                    if (err) {
                        return next(err);
                    } else {
                        generateSystemData(galaxy, universeIndex, galaxy.id.slice(-1));
                    }
                });
            }
        };

        var generateSystemData = function(galaxy, universeIndex, galaxyIndex) {
            for (var systemIndex = 0; systemIndex < entityNum; systemIndex++) {
                var systemName = 'System ' + universeIndex + '-' + galaxyIndex + '-' + systemIndex;

                var system = new System({
                    name : systemName,
                    galaxyId : galaxy._id
                });

                system.save(function(err, system) {
                    if (err) {
                        return next(err);
                    } else {
                        generateBodyData(system, universeIndex, galaxyIndex, system.id.slice(-1));
                    }
                });
            }
        };

        var generateBodyData = function(system, universeIndex, galaxyIndex, systemIndex) {
            for (var bodyIndex = 0; bodyIndex < entityNum; bodyIndex++) {
                var bodyName = 'Body ' + universeIndex + '-' + galaxyIndex + '-' + systemIndex + '-' + bodyIndex;

                var body = new Body({
                    name : bodyName,
                    systemId : system._id
                });

                body.save(function(err, body) {
                    if (err) {
                        return next(err);
                    } else {
                        generateRegionData(body, universeIndex, galaxyIndex, systemIndex, body.id.slice(-1));
                    }
                });
            }
        };

        var generateRegionData = function(body, universeIndex, galaxyIndex, systemIndex, bodyIndex) {
            for (var regionIndex = 0; regionIndex < entityNum; regionIndex++) {
                var regionName = 'Region ' + universeIndex + '-' + galaxyIndex + '-' + systemIndex + '-' + bodyIndex + '-' + regionIndex;

                var region = new Region({
                    name : regionName,
                    bodyId : body._id
                });

                region.save(function(err, region) {
                    if (err) {
                        return next(err);
                    } else {
                        generatePopulationData(region);
                    }
                });
            }
        };

        var generatePopulationData = function(region) {
            for (var populationIndex = 0; populationIndex < populationMaxAge; populationIndex++) {
                var population = new Population({
                    type : populationType,
                    age : populationIndex,
                    size : populationSize,
                    regionId : region._id
                });

                population.save(function(err) {
                    if (err) {
                        return next(err);
                    }
                });
            }
        };

        generateUniverseData();
        res.redirect('/');
    });

    param.app.get(param.urlPath, function(req, res, next) {
        res.render(param.viewPath, {});
    });
};
