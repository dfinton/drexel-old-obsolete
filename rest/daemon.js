var appRoot = require('app-root-path');
var exec = require('child_process').exec;
var path = require('path');

module.exports = function(param) {
    var payload = function(processID) {
        var isRunning = false;
        var startTime = null;

        if (processID !== null) {
            isRunning = true;
            startTime = processID.startTime;
        }

        return {
            isRunning : isRunning,
            startTime : startTime,
        };
    };

    param.rest.post(param.urlPath, function(req, content, callback) {
        var isRunningParam = ('true' === req.body.isRunning);
        var delayedResponse = function() {
            setTimeout(function() {
                param.database.model('processID').findOne({}, function(err, processID) {
                    callback(null, payload(processID));
                });
            }, 1000);
        }

        if (isRunningParam) {
            var daemonCmd = path.join(appRoot.toString(), 'bin', 'drexel');

            exec(daemonCmd, function(err, stdout) {
                delayedResponse();
            });
        } else {
            param.database.model('processID').findOne({}, function(err, processID) {
                var killCmd = null === processID ? 'echo' : 'kill ' + processID.processID;

                exec(killCmd, function(err, stdout) {
                    delayedResponse();
                });
            });
        }
    });

    param.rest.get(param.urlPath, function(req, content, callback) {
        param.database.model('processID').findOne({}, function(err, processID) {
            callback(null, payload(processID));
        });
    });
};
