module.exports = function(param) {
    var payload = function(sortedEntities, sortedChildEntities, grandchildEntities, entityType, childEntityType, grandchildEntityType) {
        var entityIdField = entityType + 'Id';
        var childEntityIdField = childEntityType + 'Id';

        for (var index = 0, len = grandchildEntities.length; index < len; index++) {
            var grandchildEntity = grandchildEntities[index];
            var childEntityId = grandchildEntity[childEntityIdField];
            var childEntity = sortedChildEntities[childEntityId];

            delete grandchildEntity[childEntityIdField];
            childEntity[grandchildEntityType + 'List'].push(grandchildEntity);
        }

        var childEntityIdList = Object.keys(sortedChildEntities);
        var entityTreeData = [];

        for (var index = 0, len = childEntityIdList.length; index < len; index++) {
            var childEntityId = childEntityIdList[index];
            var childEntity = sortedChildEntities[childEntityId];
            var entityId = childEntity[entityIdField];
            var entity = sortedEntities[entityId];
            
            delete childEntity[entityIdField];
            entity[childEntityType + 'List'].push(childEntity);
        }

        var entityIdList = Object.keys(sortedEntities);

        for (var index = 0, len = entityIdList.length; index < len; index++) {
            var entityId = entityIdList[index];
            var entity = sortedEntities[entityId];

            entityTreeData.push(entity);
        }

        return {
            hierMap : param.appConfig.entityHierarchyMap,
            entities : entityTreeData,
            entityType : entityType
        };
    };

    param.rest.get(param.urlPath, function(req, content, callback) {
        var entityType = 'universe';
        var hierMap = param.appConfig.entityHierarchyMap;

        if (req.query.hasOwnProperty('ent')) {
            entityType = req.query.ent;
        }

        if (!(hierMap.hasOwnProperty(entityType))) {
            return callback(null, {});
        }

        var childEntityType = hierMap[entityType];
        var grandchildEntityType = null;

        if (hierMap.hasOwnProperty(childEntityType)) {
            grandchildEntityType = hierMap[childEntityType];
        }

        var getEntityData = function(entityId) {
            // Generate our field list of data we want to send back
            var fieldList = [
                '_id',
                'name'
            ];

            var fields = fieldList.join(' ');

            // Generate the parameter list to filter our search results
            var parameters = {};

            if (typeof(entityId) !== 'undefined') {
                parameters._id = entityId;
            }

            // Perform the database query and send back the results
            param.database.model(entityType).find(parameters, fields).lean().exec(function(err, entities) {
                var sortedEntities = {};

                for (var index = 0, len = entities.length; index < len; index++) {
                    var entity = entities[index];

                    entity[childEntityType + 'List'] = [];
                    sortedEntities[entity._id] = entity;
                }

                getChildEntityData(sortedEntities);
            });
        };

        var getChildEntityData = function(sortedEntities) {
            // Generate our field list of data we want to send back
            var entityIdField = entityType + 'Id';

            var fieldList = [
                '_id',
                'name',
                entityIdField
            ];

            var fields = fieldList.join(' ');

            // Generate the parameter list to filter our search results
            var parameters = {};
            var idList = Object.keys(sortedEntities);

            parameters[entityIdField] = { $in : idList };

            param.database.model(childEntityType).find(parameters, fields).lean().exec(function(err, childEntities) {
                sortedChildEntities = {};

                for (var index = 0, len = childEntities.length; index < len; index++) {
                    var childEntity = childEntities[index];

                    childEntity[grandchildEntityType + 'List'] = [];
                    sortedChildEntities[childEntity._id] = childEntity;
                }

                getGrandchildEntityData(sortedEntities, sortedChildEntities);
            });
        };

        var getGrandchildEntityData = function(sortedEntities, sortedChildEntities) {
            // Generate our field list of data we want to send back
            var entityIdField = entityType + 'Id';
            var childEntityIdField = childEntityType + 'Id';

            var fieldList = [
                '_id',
                'name',
                childEntityIdField
            ];

            var fields = fieldList.join(' ');

            // Generate the parameter list to filter our search results
            var parameters = {};
            var idList = Object.keys(sortedChildEntities);

            parameters[childEntityIdField] = { $in : idList };

            param.database.model(grandchildEntityType).find(parameters, fields).lean().exec(function(err, grandchildEntities) {
                callback(null, payload(sortedEntities, sortedChildEntities, grandchildEntities, entityType, childEntityType, grandchildEntityType));
            });
        };

        if (req.query.hasOwnProperty('id')) {
            getEntityData(req.query.id);
        } else {
            getEntityData();
        }
    });
}
