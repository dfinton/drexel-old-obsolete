var error404Handler = function(req, content, callback) {
    callback(null, {}, { statusCode : 404 });
};

module.exports = function(param) {
    param.rest.del('*', error404Handler);
    param.rest.get('*', error404Handler);
    param.rest.patch('*', error404Handler);
    param.rest.post('*', error404Handler);
    param.rest.put('*', error404Handler);
};
