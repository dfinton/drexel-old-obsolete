module.exports = function(param) {
    var parentEntityId, parentEntityName;

    var getPopulationSummary = function(callback, regionIdList) {
        // Generate our field list of data we want to send back
        var fieldList = [
            'age',
            'type',
            'size'
        ];

        var fields = fieldList.join(' ');

        // Generate the parameter list to filter our search results
        var parameters = {
            regionId : { $in : regionIdList }
        };

        // Query the database to gather our population data points
        param.database.model('population').find(parameters, fields).lean().exec(function (err, populationList) {
            var typeTracker = {};
            var maxPopulationByType = {};
            var maxAgeByType = {};
            var totalPopulationByType = {};
            var populationTracker = {};
            var populationData = {};

            for (var index = 0, len = populationList.length; index < len; index++) {
                var population = populationList[index];
                var populationType = population.type;
                var populationAge = population.age;
                var populationSize = population.size;

                if (typeTracker.hasOwnProperty(populationType)) {
                    maxPopulationByType[populationType] = Math.max(populationSize, maxPopulationByType[populationType]);
                    maxAgeByType[populationType] = Math.max(populationAge, maxAgeByType[populationType]);
                    totalPopulationByType[populationType] += populationSize;

                    var currentPopulationSize = populationTracker[populationType][populationAge];

                    if ('undefined' === typeof(currentPopulationSize) || null === currentPopulationSize) {
                        populationTracker[populationType][populationAge] = populationSize;
                    } else {
                        populationTracker[populationType][populationAge] += populationSize;
                    }
                } else {
                    maxPopulationByType[populationType] = populationSize;
                    maxAgeByType[populationType] = populationAge;
                    totalPopulationByType[populationType] = populationSize;
                    populationData[populationType] = [];

                    populationTracker[populationType] = [];
                    populationTracker[populationType][populationAge] = populationSize;

                    typeTracker[populationType] = true;
                }
            }

            var populationTypeList = Object.keys(typeTracker);

            for (var typeIndex = 0, typeLen = populationTypeList.length; typeIndex < typeLen; typeIndex++) {
                var populationType = populationTypeList[typeIndex];
                var populationByAge = populationTracker[populationType];

                for (var index = 0, len = populationByAge.length; index < len; index++) {
                    var populationAge = index;
                    var populationSize = populationByAge[index];

                    if ('undefined' === typeof(populationSize) || null === populationSize) {
                        populationSize = 0;
                    }

                    populationData[populationType].push([populationAge, populationSize]);
                }
            }

            var payload = {
                _id : parentEntityId,
                name : parentEntityName,
                populationTypeList : populationTypeList,
                maxPopulationByType : maxPopulationByType,
                maxAgeByType : maxAgeByType,
                totalPopulationByType : totalPopulationByType,
                populationData : populationData
            };

            callback(null, payload);
        });
    };

    var entitySummary = function(callback, entityType, entityIdList) {
        var hierMap = param.appConfig.entityHierarchyMap;

        if (hierMap.hasOwnProperty(entityType)) {
            var childEntityType = hierMap[entityType];
            var parentIdFieldName = entityType + 'Id';

            // Generate our field list of data we want to send back
            var fieldList = [
                '_id',
                'name'
            ];

            var fields = fieldList.join(' ');

            // Generate the parameter list to filter our search results
            var parameters = {};
            
            parameters[parentIdFieldName] = { $in : entityIdList }

            // Query the database to gather our population data points
            param.database.model(childEntityType).find(parameters, fields).lean().exec(function (err, childEntityList) {
                var childEntityIdList = [];

                for (var index = 0, len = childEntityList.length; index < len; index++) {
                    childEntityIdList.push(childEntityList[index]._id);
                }

                entitySummary(callback, childEntityType, childEntityList);
            });
        } else {
            getPopulationSummary(callback, entityIdList);
        }
    };

    var parentEntitySummary = function(callback, entityType, entityId) {
        // Generate our field list of data we want to send back
        var fieldList = [
            '_id',
            'name'
        ];

        var fields = fieldList.join(' ');

        // Generate the parameter list to filter our search results
        var parameters = {
            _id : { $in : entityId }
        };

        // Query the database to gather our population data points
        param.database.model(entityType).findOne(parameters, fields).lean().exec(function (err, entity) {
            if (null === entity) {
                return callback(null, {});
            }

            parentEntityId = entity._id;
            parentEntityName = entity.name;

            entitySummary(callback, entityType, [entityId]);
        });
    };

    param.rest.get(param.urlPath, function(req, content, callback) {
        // get our required GET parameters
        if (! (req.query.hasOwnProperty('id') && req.query.hasOwnProperty('ent'))) {
            return callback(null, {});
        }

        var entityId = req.query.id;
        var entityType = req.query.ent;

        if (param.appConfig.validEntityList.indexOf(entityType) === -1) {
            return callback(null, {});
        }

        // Generate the apropriate summary based on entity type
        parentEntitySummary(callback, entityType, entityId);
    });
}
