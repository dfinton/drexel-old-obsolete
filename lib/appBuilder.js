/**
 * Automatically builds routes, models, and middleware objects from a directory tree
 **/

// Load up our core libraries
var appRoot = require('app-root-path');
var fs = require('fs'); 
var path = require('path');
var rest = require('connect-rest');

// The recursive function responsible for building routes from a directory tree
var buildExportList = function(exportRootPath, exportRelPath) {
    var exportList = [];

    var exportFileListFull = fs.readdirSync(exportRootPath);

    var exportErrorAbsPath = null;
    var exportErrorRelPath = null;
    var exportErrorUrlPath = null;

    exportRelPath = typeof(exportRelPath) === 'undefined' ? '' : exportRelPath;

    for (var exportFileIndex = 0; exportFileIndex < exportFileListFull.length; exportFileIndex++) {
        var exportFileName = exportFileListFull[exportFileIndex];
        var exportFileAbsPath = path.join(exportRootPath, exportFileName);
        var exportFileRelPath = path.join(exportRelPath, exportFileName);
        
        if ((fs.lstatSync(exportFileAbsPath).isFile()) && (/\.js$/.test(exportFileName)) && ('index.js' !== exportFileName)) {
            var exportFileNameStripped = exportFileName.match(/(.*)\.js/)[1];
            var exportFileAbsPathStripped = exportFileAbsPath.match(/(.*)\.js/)[1];
            var exportFileRelPathStripped = exportFileRelPath.match(/(.*)\.js/)[1];

            var exportUrlPath = '/' + ('home' === exportFileNameStripped ? exportRelPath : exportFileRelPathStripped);

            if (/^error$/.test(exportFileNameStripped)) {
                exportErrorAbsPath = exportFileAbsPathStripped;
                exportErrorRelPath = exportFileRelPathStripped;
                exportErrorUrlPath = exportUrlPath;
            } else {
                var exportFunc = require(exportFileAbsPathStripped);

                exportList.push({
                    func : exportFunc,
                    relPath : exportFileRelPathStripped,
                    urlPath : exportUrlPath
                });
            }
        } else if (fs.lstatSync(exportFileAbsPath).isDirectory()) {
            var exportSubList = buildExportList(exportFileAbsPath, exportFileRelPath);

            exportList = exportList.concat(exportSubList);
        }
    }

    if (null !== exportErrorAbsPath) {
        var exportFunc = require(exportErrorAbsPath);

        exportList.push({
            func : exportFunc,
            relPath : exportErrorRelPath,
            urlPath : exportErrorUrlPath
        });
    }

    return exportList;
};

// This will build regular express routes and incorporate them into our app
routeExport = function(app, debug, middleware, database, passport, appConfig, routePath) {
    var routePath = (typeof(routePath) === 'undefined') ? 'routes' : routePath;
    var routeRootPath = path.join(appRoot.toString(), routePath);
    var routeExportList = buildExportList(routeRootPath);

    // Call any global middleware here
    app.use(middleware.updatePlayerActivity);

    for (routeExportIndex = 0; routeExportIndex < routeExportList.length; routeExportIndex++) {
        var routeExport = routeExportList[routeExportIndex];
        var routeFunc = routeExport.func;
        var routeRelPath = routeExport.relPath;
        var routeUrlPath = routeExport.urlPath;

        routeFunc({
            app : app, 
            debug : debug,
            database : database,
            urlPath : routeUrlPath, 
            viewPath : routeRelPath, 
            middleware : middleware,
            passport : passport,
            appConfig : appConfig
        });
    }
};

// This will build RESTful routes and incorporate them into our app
restExport = function(app, debug, database, restConfig, appConfig, restPath) {
    restPath = (typeof(restPath) === 'undefined') ? 'rest' : restPath;

    var restRootPath = path.join(appRoot.toString(), restPath);
    var restExportList = buildExportList(restRootPath);
    var rester = rest.rester(restConfig);

    app.use(rester);

    for (restExportIndex = 0; restExportIndex < restExportList.length; restExportIndex++) {
        var restExport = restExportList[restExportIndex];
        var restFunc = restExport.func;
        var restRelPath = restExport.relPath;
        var restUrlPath = restExport.urlPath;

        restFunc({
            rest : rest, 
            debug : debug,
            urlPath : restUrlPath,
            database : database,
            appConfig : appConfig
        });
    }
};

// Return the methods
module.exports = {
    routes : routeExport,
    rest : restExport,
};
