module.exports = function(database, debug, appConfig) {
    var populationAge = function() {
        var conditions = {
            _id : {
                $exists : true
            }
        };

        var update = {
            $inc : { age : 1 }
        };
        
        var parameters = {
            multi : true
        };

        database.model('population').update(conditions, update, parameters, function(err, populationList) {
            if (err) {
                debug("An error happened aging an existing population!");
            }
        });
    };

    var populationBirth = function() {
        populationSize = 10000;

        // Generate our field list of data we want to send back
        var fieldList = [
            'type',
            'regionId'
        ];

        var fields = fieldList.join(' ');

        // Generate the parameter list to filter our search results
        var parameters = {};

        // Query the database
        var populationStream = database.model('population').find(fields, parameters).stream();

        populationStream.on('data', function(population) {
            debug("Found one!");
        }).on('error', function(err) {
            debug("Error!");
        }).on('close', function() {
            debug("Stream closed!");
        });
    };

    return function() {
        debug("Starting new iteration");
        populationAge();
        populationBirth();
    };
}
