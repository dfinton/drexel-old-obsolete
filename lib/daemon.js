module.exports = function(database) {
    var ProcessID = database.model('processID');

    var daemonMethods = {
        'start': function() {
            ProcessID.findOne({}, function(err, processID) {
                if (null === processID) {
                    var newProcessID = new ProcessID({
                        processID : process.pid,
                    });

                    newProcessID.save(function(err) {
                        if (err) {
                            process.exit(2);
                        }
                    });
                } else {
                    process.exit(1);
                }
            });
        },

        'stop' : function() {
            ProcessID.findOne({}, function(err, processID) {
                if (null === processID) {
                    process.exit(2);
                } else {
                    ProcessID.findOne({}, function(error, processID) {
                        processID.remove();
                        process.exit(0);
                    });
                }
            });
        },

        'status' : function() {

        },
    };

    return daemonMethods;
};
